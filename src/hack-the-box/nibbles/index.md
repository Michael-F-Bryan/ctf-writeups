# Nibbles

Challenge Info:

- **Type:** machine
- **IP address:** 10.10.10.75
- **Base Points:** 20
- **Difficulty:** Easy (3.6/10)
- **OS:** Linux
- **Profile:** https://www.hackthebox.eu/home/machines/profile/121

## Enumeration

First we start off with a simple `nmap` scan:

```console
$ nmap 10.10.10.75
Starting Nmap 7.70 ( https://nmap.org ) at 2018-06-16 07:50 BST
Nmap scan report for 10.10.10.75
Host is up (0.39s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 9.93 seconds
```

Inspecting the HTTP server shows there is a blog running on port 80. The
blog is built using [nibbleblog], a simple blogging engine written in PHP.

The link isn't visible from the web page, but admins can login via 
http://10.10.10.75/nibbleblog/admin.php and the default user is `admin`.

It looks like whoever set up the website didn't configure apache correctly 
because you can see which files are on the file system by navigating to any 
valid folder. For example:

- http://10.10.10.75/nibbleblog/content/
- http://10.10.10.75/nibbleblog/admin/

These directories contain a bunch of `*.php` and `*.bit` files which make up the
blogging engine. The `*.bit` files are just normal PHP files that nibbleblog
includes and executes.

Opening the `*.php` files shows no contents. Initially I thought there was some
sort of security mechanism protecting PHP files (there's a blacklister thing
which will block your IP if you try to bruteforce the admin login), but it turns
out things were a lot simpler and apache was executing the files. They just
didn't generate any output.

## Exploitation

I initially tried a bunch of username/password combinations and couldn't figure
out how to get into the admin area. I even scoured through their github repo,
looking for a default password or some other backdoor into the system.

After getting frustrated and walking away, I came across [a forum thread] where
one of the commenters hinted "*machine names usually mean something*". Then it
clicked that, like the other commenters were saying, I'd been overthinking 
things. Using `admin` and `nibbler` was enough to get in.

First thing I did once I'd entered the admin panel was have a snoop around the
various tabs and pages.

On the "publish" page we are presented with a standard editor. One of the editor
options that piqued my curiosity was "nibblerblog code" which looked like it
lets you insert arbitrary code. 

I'm still not sure whether "nibblerblog code" will actually execute arbitrary
code, or merely render it with syntax highlighting because the next thing I
did was google it. The first option was [a blog post] publicising 
*NibblerBlog Code Execution*.

The *NibblerBlog Code Execution* post basically shows that you can use the
`my_image` plugin to upload arbitrary files and it *doesn't* check to file
extension to ensure you only upload valid images. From here it's just a case of
uploading an arbitrary PHP script to read the flag and then open our "image" in
the browser, causing apache to execute it.

```php
<?php 
	$home_dir = "/home/" . get_current_user();
	system("ls " . $home_dir);

	echo file_get_contents($home_dir . "/user.txt");
?>

```

[nibbleblog]: http://www.nibbleblog.com/
[a forum thread]: https://forum.hackthebox.eu/discussion/391/nibbles/p1
[a blog post]: https://curesec.com/blog/article/blog/NibbleBlog-403-Code-Execution-47.html
