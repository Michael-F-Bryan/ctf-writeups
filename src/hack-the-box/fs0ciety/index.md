# Fs0ciety

Challenge Info:

- **Type:** Misc Challenges
- **Zip Password:** hackthebox
- **Sha256:** 1b117d42978f9a0f0e6460f654bca51da8dedaa82a1ef540cd6253f109632dc8
- **Base Points:** 30
- **Difficulty:** Easy 

> We believe that there is an SSH Password inside password protected 'ZIP'
> folder. Can you crack the 'ZIP' folder and get the SSH password?

## Exploitation

We're presented with an encrypted zip file, so the logical first step is to try
and brute-force the password.

```console
$ fcrackzip -u -D -p /usr/share/wordlists/rockyou.txt fsociety.zip                  

PASSWORD FOUND!!!!: pw == justdoit
```

That was easy. Now lets check out the archive's contents.

```console
$ unzip fsociety.zip
Archive:  fsociety.zip
[fsociety.zip] sshcreds_datacenter.txt password: <insert-password-here>
  inflating: sshcreds_datacenter.txt 

$ cat sshcreds_datacenter.txt 
*****************************************************************************************
Encrypted SSH credentials to access Blume ctOS : 

MDExMDEwMDEgMDExMDAxMTAgMDEwMTExMTEgMDExMTEwMDEgMDAxMTAwMDAgMDExMTAxMDEgMDEwMTExMTEgMDExMDAwMTEgMDEwMDAwMDAgMDExMDExMTAgMDEwMTExMTEgMDAxMDAxMDAgMDExMDExMDEgMDAxMTAwMTEgMDExMDExMDAgMDExMDExMDAgMDEwMTExMTEgMDExMTAxMTEgMDExMDEwMDAgMDEwMDAwMDAgMDExMTAxMDAgMDEwMTExMTEgMDExMTAxMDAgMDExMDEwMDAgMDAxMTAwMTEgMDEwMTExMTEgMDExMTAwMTAgMDAxMTAwMDAgMDExMDAwMTEgMDExMDEwMTEgMDEwMTExMTEgMDExMDEwMDEgMDExMTAwMTEgMDEwMTExMTEgMDExMDAwMTEgMDAxMTAwMDAgMDAxMTAwMDAgMDExMDEwMTEgMDExMDEwMDEgMDExMDExMTAgMDExMDAxMTE=

*****************************************************************************************
```

Although the contents looks strange (what's with all the `MDE` and `MTA`s?),
that trailing `=` makes me think of base-64 encoding...

```console
$ echo $encrypted | base64 -d             
01101001 01100110 01011111 01111001 00110000 01110101 01011111 01100011
01000000 01101110 01011111 00100100 01101101 00110011 01101100 01101100
01011111 01110111 01101000 01000000 01110100 01011111 01110100 01101000
00110011 01011111 01110010 00110000 01100011 01101011 01011111 01101001
01110011 01011111 01100011 00110000 00110000 01101011 01101001 01101110
01100111
```

Now we just need to pull up python and convert that "binary" into something more
readable.

```python
$ ipython3

In [1]: data = "01101001 01100110 01011111 01111001 00110000 01110101 01011111 " +\
   ...: "01100011 01000000 01101110 01011111 00100100 01101101 00110011 01101100 " +\
   ...: "01101100 01011111 01110111 01101000 01000000 01110100 01011111 01110100 " +\
   ...: "01101000 00110011 01011111 01110010 00110000 01100011 01101011 01011111 " +\
   ...: "01101001 01110011 01011111 01100011 00110000 00110000 01101011 01101001 " +\
   ...: "01101110 01100111"

In [2]: letters = [chr(int(word, 2)) for word in data.split()]

In [3]: "".join(letters)
Out[3]: 'if_y0u_c@n_$m3ll_wh@t_th3_r0ck_is_c00king'
```

And now we've got our flag: `HTB{if_y0u_c@n_$m3ll_wh@t_th3_r0ck_is_c00king}`.